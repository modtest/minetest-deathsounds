-- Engine bug calls register_on_dieplayer get called repeatedly after death by fire/lava
-- Waiting until the play respawns to reenable deathsounds is a workaround.

local can_play_deathsound = {}

minetest.register_on_joinplayer(function(player)
    can_play_deathsound[player:get_player_name()] = true
end
)

minetest.register_on_dieplayer(function(player)
    local name = player:get_player_name()
    
    if can_play_deathsound[name] == true then
        minetest.sound_play("deathsounds_death", {
            to_player = name,
            gain = 2.0
        })

        can_play_deathsound[name] = false
    end
end
)

minetest.register_on_respawnplayer(function(player)
    can_play_deathsound[player:get_player_name()] = true
end
)

minetest.register_on_chat_message(function(name, message)
        if message:lower() == "i have information that will lead to hillary clinton's arrest." then 
            minetest.get_player_by_name(name):set_hp(0)
            minetest.chat_send_all(name .. " has mysteriously disapeared.")
        end
    end
)
